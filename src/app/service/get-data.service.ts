import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

// import {Fachbereich, getEmptyCase} from '../model/fachbereich-info';

@Injectable({
  providedIn: 'root',
})
export class GetDataService {
  constructor(private http: HttpClient) {}

  private readonly fachbereichDataSubject = new BehaviorSubject<any>(null);

  readonly fachbereichData$ = this.fachbereichDataSubject.asObservable();

  public getFachbereich() {
    const url = 'http://localhost:5000/fachbereich';
    this.http.get<any>(url).subscribe((response) => {
      this.fachbereichDataSubject.next(response);
    });
  }
}
