export interface Fachbereich {
  name?: string;
}

export function getEmptyCase(): Fachbereich {
  return {
    name: '',
  };
}
