import { Component, OnInit } from '@angular/core';
import { GetDataService } from '../service/get-data.service';

@Component({
  selector: 'app-user',
  templateUrl: './fachbereich.component.html',
  styleUrls: ['./fachbereich.component.scss'],
})
export class FachbereichComponent implements OnInit {
  readonly caseData$ = this.getDataService.fachbereichData$;
  constructor(public getDataService: GetDataService) {}

  ngOnInit() {
    this.getDataService.getFachbereich();
  }
}
