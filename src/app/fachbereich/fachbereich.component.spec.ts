import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FachbereichComponent } from './fachbereich.component';

describe('FachbereichComponent', () => {
  let component: FachbereichComponent;
  let fixture: ComponentFixture<FachbereichComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FachbereichComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FachbereichComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
